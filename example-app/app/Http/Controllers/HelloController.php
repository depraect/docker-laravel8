<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HelloController extends Controller
{

    public function index(){
        return response()->json(['ping'=>'pong'],200);
    }

    public function adios(Request  $request, $idUsuario)
    {

        return response()->json(['el id del usuario'=>$idUsuario],200);
    }
}
